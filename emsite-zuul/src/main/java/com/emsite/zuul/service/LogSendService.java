package com.emsite.zuul.service;

import com.netflix.zuul.context.RequestContext;

/**
 * @Description: 日志消息发往消息队列工具类
 * @author lix
 * @date 2018/8/20 14:36
 */
public interface LogSendService {
    /**
     * 往消息通道发消息
     *
     * @param requestContext requestContext
     */
    void send(RequestContext requestContext);
}
