package com.emsite.zuul.feign.fallback;

import com.emsite.common.vo.MenuVO;
import com.emsite.zuul.feign.MenuService;
import com.xiaoleilu.hutool.collection.CollUtil;
import javafx.scene.input.DataFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.util.Date;
import java.util.Set;

/**
* @Description: 菜单熔断
* @author lix
* @date 2018/8/20 14:30
*/
@Slf4j
@Service
public class MenuServiceFallbackImpl implements MenuService {
    @Override
    public Set<MenuVO> findMenuByRole(String role) {
        log.error("调用{}异常{} 时间{}","findMenuByRole",role,new Date());
        return CollUtil.newHashSet();
    }
}
