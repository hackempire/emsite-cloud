package com.emsite.upms.service;
import com.baomidou.mybatisplus.service.IService;
import com.emsite.upms.model.entity.SysRoleMenu;


public interface SysRoleMenuService extends IService<SysRoleMenu> {
    /**
     * 更新角色菜单
     * @param role
     * @param roleId  角色
     * @param menuIds 菜单列表
     * @return
     */
    Boolean insertRoleMenus(String role, Integer roleId, Integer[] menuIds);
}
