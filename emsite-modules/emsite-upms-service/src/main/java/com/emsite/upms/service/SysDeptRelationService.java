package com.emsite.upms.service;

import com.baomidou.mybatisplus.service.IService;
import com.emsite.upms.model.entity.SysDeptRelation;
/**
* @Description: 部门间关系服务接口
* @author lix
* @date 2018/8/20 15:19
*/
public interface SysDeptRelationService extends IService<SysDeptRelation> {
	
}
