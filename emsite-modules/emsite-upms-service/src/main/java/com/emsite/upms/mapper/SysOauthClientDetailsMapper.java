package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysOauthClientDetails;

public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
