package com.emsite.upms.service.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.upms.mapper.SysRoleDeptMapper;
import com.emsite.upms.model.entity.SysRoleDept;
import com.emsite.upms.service.SysRoleDeptService;
import org.springframework.stereotype.Service;
/**
* @Description: 角色与部门对应关系 服务实现类
* @author lix
* @date 2018/8/20 15:18
*/
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements SysRoleDeptService {
	
}
