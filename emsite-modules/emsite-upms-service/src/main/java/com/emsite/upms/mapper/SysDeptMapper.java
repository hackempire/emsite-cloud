package com.emsite.upms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysDept;

import java.util.List;

public interface SysDeptMapper extends BaseMapper<SysDept> {
    void deleteDept(List<Integer> list);
}