package com.emsite.upms.model.dto;
import lombok.Data;

/**
* @Description: 部门树
* @author lix
* @date 2018/8/20 15:11
*/
@Data
public class DeptTree extends TreeNode {
    private String name;
}
