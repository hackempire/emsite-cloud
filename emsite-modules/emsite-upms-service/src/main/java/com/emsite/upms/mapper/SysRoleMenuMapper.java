package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysRoleMenu;

public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}