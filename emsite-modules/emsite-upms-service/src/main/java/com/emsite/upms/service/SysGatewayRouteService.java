package com.emsite.upms.service;
import com.baomidou.mybatisplus.service.IService;
import com.emsite.common.entity.SysGatewayRoute;


public interface SysGatewayRouteService extends IService<SysGatewayRoute> {
    /**
     * 立即生效配置
     * @return
     */
    Boolean applyZuulRoute();
}
