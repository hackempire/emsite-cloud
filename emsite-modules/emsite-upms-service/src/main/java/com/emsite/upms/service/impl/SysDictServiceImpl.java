package com.emsite.upms.service.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.upms.mapper.SysDictMapper;
import com.emsite.upms.model.entity.SysDict;
import com.emsite.upms.service.SysDictService;
import org.springframework.stereotype.Service;
/**
* @Description: 字典表 服务实现类
* @author lix
* @date 2018/8/20 15:17
*/
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

}
