package com.emsite.upms.service;
import com.baomidou.mybatisplus.service.IService;
import com.emsite.upms.model.entity.SysDict;

public interface SysDictService extends IService<SysDict> {

}
