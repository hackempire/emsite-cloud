package com.emsite.upms.service.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.common.constant.CommonConstant;
import com.emsite.common.entity.SysLog;
import com.emsite.upms.mapper.SysLogMapper;
import com.emsite.upms.service.SysLogService;
import com.xiaoleilu.hutool.lang.Assert;
import org.springframework.stereotype.Service;

import java.util.Date;
/**
* @Description: 日志表 服务实现类
* @author lix
* @date 2018/8/20 15:17
*/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

    @Override
    public Boolean updateByLogId(Long id) {
        Assert.isNull(id, "日志ID为空");
        SysLog sysLog = new SysLog();
        sysLog.setId(id);
        sysLog.setDelFlag(CommonConstant.STATUS_DEL);
        sysLog.setUpdateTime(new Date());
        return updateById(sysLog);
    }
}
