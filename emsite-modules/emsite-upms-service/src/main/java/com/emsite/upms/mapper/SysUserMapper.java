package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.common.bean.interceptor.DataScope;
import com.emsite.common.utils.QPage;
import com.emsite.common.vo.UserVO;
import com.emsite.upms.model.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper extends BaseMapper<SysUser> {

    UserVO selectUserVoByUsername(String username);

    List selectUserVoPageDataScope(QPage query, @Param("username") Object username,
                                   @Param("status") Object status, DataScope dataScope);

    UserVO selectUserVoByMobile(String mobile);

    UserVO selectUserVoByOpenId(String openId);

    UserVO selectUserVoById(Integer id);
}