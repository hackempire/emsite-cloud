package com.emsite.upms.controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.emsite.common.constant.CommonConstant;
import com.emsite.common.web.BaseController;
import com.emsite.upms.model.dto.DeptTree;
import com.emsite.upms.model.entity.SysDept;
import com.emsite.upms.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
* @Description: 部门管理
* @author lix
* @date 2018/8/20 15:06
*/
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController {
    @Autowired
    private SysDeptService sysDeptService;

    /**
     * 通过ID查询
     */
    @GetMapping("/{id}")
    public SysDept get(@PathVariable Integer id) {
        return sysDeptService.selectById(id);
    }

    /**
    * @Description: 返回机构树
    * @author lix
    * @date 2018/8/20 15:05
    */
    @GetMapping(value = "/tree")
    public List<DeptTree> getTree() {
        SysDept condition = new SysDept();
        condition.setDelFlag(CommonConstant.STATUS_NORMAL);
        return sysDeptService.selectListTree(new EntityWrapper<>(condition));
    }

    /**
    * @Description: 新增
    * @author lix
    * @date 2018/8/20 15:06
    */
    @PostMapping
    public Boolean add(@RequestBody SysDept  sysDept) {
        return sysDeptService.insertDept(sysDept);
    }

    /**
    * @Description: 删除
    * @author lix
    * @date 2018/8/20 15:06
    */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Integer id) {
        return sysDeptService.deleteDeptById(id);
    }

    /**
    * @Description: 修改
    * @author lix
    * @date 2018/8/20 15:06
    */
    @PutMapping
    public Boolean edit(@RequestBody SysDept sysDept) {
        sysDept.setUpdateTime(new Date());
        return sysDeptService.updateDeptById(sysDept);
    }
}
