package com.emsite.upms.model.entity;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
* @Description: 部门间关系
* @author lix
* @date 2018/8/20 15:13
*/
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("sys_dept_relation")
public class SysDeptRelation extends Model<SysDeptRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * 祖先节点
     */
	private Integer ancestor;
    /**
     * 后代节点
     */
	private Integer descendant;


	@Override
	protected Serializable pkVal() {
		return this.ancestor;
	}

	@Override
	public String toString() {
		return "SysDeptRelation{" +
			", ancestor=" + ancestor +
			", descendant=" + descendant +
			"}";
	}
}
