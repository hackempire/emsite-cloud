package com.emsite.upms.service;

import com.baomidou.mybatisplus.service.IService;
import com.emsite.upms.model.entity.SysOauthClientDetails;


public interface SysOauthClientDetailsService extends IService<SysOauthClientDetails> {
    /**
     ============================================
     * 描   述： 新增
     * 参   数： sysOauthClientDetails
     * 返回类型：boolean
     * 创 建 人：Jerry
     * 创建时间：2018/8/14 10:00
     ============================================
     */
    @Override
    boolean insert(SysOauthClientDetails sysOauthClientDetails);
    /**
     ============================================
     * 描   述：编辑
     * 参   数： sysOauthClientDetails
     * 返回类型：boolean
     * 创 建 人：Jerry
     * 创建时间：2018/8/14 10:00
     ============================================
     */
    @Override
    boolean updateById(SysOauthClientDetails sysOauthClientDetails);
}
