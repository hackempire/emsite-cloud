package com.emsite.upms.model.dto;

import com.emsite.upms.model.entity.SysRole;
import lombok.Data;

/**
* @Description: 角色DTO
* @author lix
* @date 2018/8/20 15:11
*/
@Data
public class RoleDTO extends SysRole {
    /**
     * 角色部门Id
     */
    private Integer roleDeptId;

    /**
     * 部门名称
     */
    private String deptName;
}
