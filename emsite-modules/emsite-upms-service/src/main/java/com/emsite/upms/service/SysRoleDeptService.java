package com.emsite.upms.service;

import com.baomidou.mybatisplus.service.IService;
import com.emsite.upms.model.entity.SysRoleDept;


public interface SysRoleDeptService extends IService<SysRoleDept> {
	
}
