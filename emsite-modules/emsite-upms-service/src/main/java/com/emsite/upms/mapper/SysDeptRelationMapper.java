package com.emsite.upms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysDeptRelation;

import java.util.List;


public interface SysDeptRelationMapper extends BaseMapper<SysDeptRelation> {

    List<Integer> selectDeptRelaIds(Integer id);

    void deleteDeptRealtion(List<Integer> list);
}