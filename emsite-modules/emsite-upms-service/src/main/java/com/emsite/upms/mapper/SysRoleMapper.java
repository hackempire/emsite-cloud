package com.emsite.upms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.common.utils.QPage;
import com.emsite.upms.model.entity.SysRole;

import java.util.List;
import java.util.Map;

public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<Object> selectRolePage(QPage<Object> query, Map<String, Object> condition);

    List<SysRole> selectListByDeptId(Integer deptId);
}