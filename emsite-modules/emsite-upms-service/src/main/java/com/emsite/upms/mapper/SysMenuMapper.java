package com.emsite.upms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.common.vo.MenuVO;
import com.emsite.upms.model.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<MenuVO> findMenuByRoleName(@Param("role") String role);
}