package com.emsite.upms.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
/**
* @Description: 组织树节点
* @author lix
* @date 2018/8/20 15:12
*/
@Data
public class TreeNode {
    protected int id;
    protected int parentId;
    protected List<TreeNode> children = new ArrayList<TreeNode>();
    public void add(TreeNode node) {
        children.add(node);
    }
}
