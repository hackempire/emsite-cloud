package com.emsite.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
/**
* @Description: 授权启动类
* @author lix
* @date 2018/8/20 15:01
*/
@EnableFeignClients
@ComponentScan(basePackages = {"com.emsite.auth", "com.emsite.common.bean"})
@EnableDiscoveryClient
@SpringBootApplication
public class EmsiteAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmsiteAuthApplication.class, args);
    }
}
