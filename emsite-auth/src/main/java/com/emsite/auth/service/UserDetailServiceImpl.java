package com.emsite.auth.service;

import com.emsite.auth.feign.UserService;
import com.emsite.auth.util.UserDetailsImpl;
import com.emsite.common.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
/**
* @Description: 认证数据访问
* @author lix
* @date 2018/8/20 15:01
*/
@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    public UserDetailsImpl loadUserByUsername(String username){
        UserVO userVo = userService.findUserByUsername(username);
        if(null == userVo){
            throw new UsernameNotFoundException("用户不存在");
        }
        return new UserDetailsImpl(userVo);
    }
}