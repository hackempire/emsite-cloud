package com.emsite.auth.feign;

import com.emsite.auth.feign.fallback.UserServiceFallbackImpl;
import com.emsite.common.vo.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
* @Description: 用户信息服务
* @author lix
* @date 2018/8/20 14:59
*/
@FeignClient(name = "emsite-upms", fallback = UserServiceFallbackImpl.class)
public interface UserService {
    /**
     ============================================
     * 描   述：通过用户名查询用户、角色信息
     * 参   数：[username]
     * 返回类型：UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:48
     ============================================
     */
    @GetMapping("/user/findUserByUsername/{username}")
    UserVO findUserByUsername(@PathVariable("username") String username);


    /**
     ============================================
     * 描   述：通过手机号查询用户、角色信息
     * 参   数：[mobile]
     * 返回类型：UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:48
     ============================================
     */
    @GetMapping("/user/findUserByMobile/{mobile}")
    UserVO findUserByMobile(@PathVariable("mobile") String mobile);


    /**
     ============================================
     * 描   述：根据OpenId查询用户信息
     * 参   数：[openId]
     * 返回类型：UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:49
     ============================================
     */
    @GetMapping("/user/findUserByOpenId/{openId}")
    UserVO findUserByOpenId(@PathVariable("openId") String openId);
}
