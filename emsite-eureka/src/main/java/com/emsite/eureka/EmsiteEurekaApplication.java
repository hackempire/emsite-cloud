package com.emsite.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EmsiteEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmsiteEurekaApplication.class, args);
    }
}
