package com.emsite.turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableTurbine
@SpringBootApplication
public class EmsiteTurbineApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmsiteTurbineApplication.class, args);
    }
}
