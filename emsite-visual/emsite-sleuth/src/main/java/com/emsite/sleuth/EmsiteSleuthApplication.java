package com.emsite.sleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmsiteSleuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmsiteSleuthApplication.class, args);
    }
}
