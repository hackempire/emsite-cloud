package com.emsite.common.vo;

import com.emsite.common.entity.SysLog;
import lombok.Data;

import java.io.Serializable;
/**
* @Description: 日志数据传输模型
* @author lix
* @date 2018/8/20 14:48
*/
@Data
public class LogVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private SysLog sysLog;
    private String username;
}
