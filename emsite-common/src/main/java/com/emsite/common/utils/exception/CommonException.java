package com.emsite.common.utils.exception;
/**
* @Description: 异常信息封装
* @author lix
* @date 2018/8/20 14:46
*/
public class CommonException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CommonException() {
    }

    public CommonException(String message) {
        super(message);
    }

    public CommonException(Throwable cause) {
        super(cause);
    }

    public CommonException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

